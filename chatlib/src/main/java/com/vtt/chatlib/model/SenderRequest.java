package com.vtt.chatlib.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by HaiKE on 10/18/17.
 */

public class SenderRequest implements Serializable {

  @SerializedName("domain")
  private String domain;
  private final static long serialVersionUID = 3144491039882960506L;
  @SerializedName("data")
  private Object data;

  public SenderRequest(String domain, Object data) {
    this.domain = domain;
    this.data = data;
  }

  public String getDomain() {
    return domain;
  }

  public void setDomain(String domain) {
    this.domain = domain;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "SenderRequest{" +
        "domain='" + domain + '\'' +
        '}';
  }
}