package com.vtt.chatlib.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Status implements Serializable {

    @SerializedName("code")
    @Expose
    private long code;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("description")
    @Expose
    private Object description;
    private final static long serialVersionUID = 7404740662749069384L;

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Status{" +
                "code=" + code +
                ", type='" + type + '\'' +
                ", description=" + description +
                '}';
    }
}
