package com.vtt.chatlib.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by HaiKE on 10/18/17.
 */

public class ChatBotRequest implements Serializable {

  @SerializedName("sender")
  @Expose
  private SenderRequest sender;
  @SerializedName("message")
  @Expose
  private MessageRequest message;
  @SerializedName("context")
  @Expose
  private Object context;

  private final static long serialVersionUID = -1520719562866582978L;

  public ChatBotRequest(SenderRequest sender, MessageRequest message, Object context) {
    this.sender = sender;
    this.message = message;
    this.context = context;
  }

  public SenderRequest getSender() {
    return sender;
  }

  public void setSender(SenderRequest sender) {
    this.sender = sender;
  }

  public MessageRequest getMessage() {
    return message;
  }

  public void setMessage(MessageRequest message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return "ChatBotRequest{" +
        "sender=" + sender +
        ", message=" + message +
        '}';
  }
}