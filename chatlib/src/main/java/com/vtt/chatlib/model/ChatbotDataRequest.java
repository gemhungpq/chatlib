package com.vtt.chatlib.model;

import com.google.gson.annotations.SerializedName;

public class ChatbotDataRequest {
    private String appDialogId;
    @SerializedName("account")
    private String account;
    @SerializedName("mode")
    private String mode;
    @SerializedName("ttv_id")
    private String ttvId;
    @SerializedName("msisdn")
    private String msisdn;
    @SerializedName("subchannel")
    private Subchannel subchannel;

    public ChatbotDataRequest(String appDialogId, String account, String mode, String ttvId, String msisdn, Subchannel subchannel) {
        this.appDialogId = appDialogId;
        this.account = account;
        this.mode = mode;
        this.ttvId = ttvId;
        this.msisdn = msisdn;
        this.subchannel = subchannel;
    }

    public static ChatbotDataRequest getDefault() {
        Subchannel subchannel = new Subchannel("INTERNAL/3RD_PARTY", "<SUBCHANNEL_NAME>");
        return new ChatbotDataRequest("APP_DIALOG_ID",
                "USER_ACCOUNT", "NORMAL", "cskh123", "USER_PHONE_NUMBER", subchannel);
    }
}
