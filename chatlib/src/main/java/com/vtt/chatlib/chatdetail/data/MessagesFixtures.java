package com.vtt.chatlib.chatdetail.data;

import com.vtt.chatlib.R;
import com.vtt.chatlib.chatdetail.data.model.Message;
import com.vtt.chatlib.chatdetail.data.model.User;
import com.vtt.chatlib.chatkit.models.Button;
import com.vtt.chatlib.chatkit.models.ChartArea;
import com.vtt.chatlib.chatkit.models.ChartColumn;
import com.vtt.chatlib.chatkit.models.ChartLine;
import com.vtt.chatlib.chatkit.models.ChartStack;
import com.vtt.chatlib.chatkit.models.QuickReply;
import com.vtt.chatlib.chatkit.models.ChartCircle;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/*
 * Created by troy379 on 12.12.16.
 */
public final class MessagesFixtures extends FixturesData {
  private MessagesFixtures() {
    throw new AssertionError();
  }

  public static Message getImageMessage() {
    Message message = new Message(getRandomId(), getBotUser(), null);
    message.setImage(new Message.Image("http://cafefcdn.com/2016/vnm-1482297205884.png"));
    return message;
  }

  public static Message getImageLoadingMessage() {
    Message message = new Message(getRandomId(), getBotUser(), null);
    message.setImageLoading(new Message.Image(R.raw.ic_chat_loading + ""));
    return message;
  }

  public static Message getVoiceMessage() {
    Message message = new Message(getRandomId(), getUser(), null);
    message.setVoice(new Message.Voice("http://example.com", rnd.nextInt(200) + 30));
    return message;
  }

  public static Message getLinkMessage() {
    Message message = new Message(getRandomId(), getBotUser(), null);
    message.setMessageLink(new Message.Link("http://www.excel-easy.com/data-analysis/images/charts/column-chart.png", "http://www.excel-easy.com/data-analysis/charts.html", "Test chat bot link demo!"));
    return message;
  }

  public static Message getActionMessage() {
    Message message = new Message(getRandomId(), getBotUser(), null);

    ArrayList<QuickReply> actionList = new ArrayList<>();
    QuickReply action = new QuickReply("text", "4G70", "abc");
    QuickReply action1 = new QuickReply("text", "4G90", "abc");
    QuickReply action2 = new QuickReply("text", "MIMAX", "abc");
    QuickReply action3 = new QuickReply("text", "MIMAXSV", "abc");
    QuickReply action4 = new QuickReply("text", "MIMAX15", "abc");

    actionList.add(action);
    actionList.add(action1);
    actionList.add(action2);
    actionList.add(action3);
    actionList.add(action4);

    Message.QuickReply messageAction = new Message.QuickReply(actionList);
    message.setMessageAction(messageAction);
    message.setText("Mời bạn nhập hoặc lựa chọn gói cước để Viettel tư vấn");

    return message;
  }

  public static Message getButtonMessage() {
    Message message = new Message(getRandomId(), getBotUser(), null);

    ArrayList<QuickReply> actionList = new ArrayList<>();
    QuickReply action = new QuickReply("text", "4G70", "abc");
    QuickReply action1 = new QuickReply("text", "4G90", "abc");
    QuickReply action2 = new QuickReply("text", "MIMAX", "abc");
    QuickReply action3 = new QuickReply("text", "MIMAXSV", "abc");
    QuickReply action4 = new QuickReply("text", "MIMAX15", "abc");

    actionList.add(action);
    actionList.add(action1);
    actionList.add(action2);
    actionList.add(action3);
    actionList.add(action4);

    ArrayList<Button> buttonList = new ArrayList<>();
    Button button = new Button("Goi 3G", "50k", actionList);
    Button button1 = new Button("Goi 3G1", "50k", actionList);
    Button button2 = new Button("Goi 3G2", "50k", actionList);
    Button button3 = new Button("Goi 3G3", "50k", actionList);

    buttonList.add(button);
    buttonList.add(button1);
    buttonList.add(button2);
    buttonList.add(button3);

    Message.Button messageAction = new Message.Button(buttonList);
    message.setMessageButton(messageAction);
    message.setText("Mời bạn nhập hoặc lựa chọn gói cước để Viettel tư vấn");

    return message;
  }

  public static Message getChartCircleMessage() {
    Message message = new Message(getRandomId(), getBotUser(), null);

    ArrayList<ChartCircle> actionList = new ArrayList<>();
    ChartCircle action = new ChartCircle("DT thẻ cào", "#35CBC5", 561607, 56.38f, 131);
    ChartCircle action1 = new ChartCircle("CPS trả sau", "#29A19C", 307089, 30.83f, 147);
    ChartCircle action2 = new ChartCircle("Kết nối đến trong nước", "#F0D260", 67074, 6.73f, 152);
    ChartCircle action3 = new ChartCircle("DT thanh toán cước TT qua B+", "#4BAE4F", 19892, 2f, 137);
    ChartCircle action4 = new ChartCircle("DT TBĐC di động", "#F95557", 15070, 1.51f, 138);
    ChartCircle action5 = new ChartCircle("Pay199 thanh toán cho TS(trừ)", "#2095F2", 10957, 1.09f, 151);
    ChartCircle action6 = new ChartCircle("DT kit", "#2095F2", 8526, 0.86f, 130);
    ChartCircle action7 = new ChartCircle("DT TTPBH qua Anypay", "#99A5CB", 2581, 0.26f, 136);
    ChartCircle action8 = new ChartCircle("DT GTGT ngoài TKG", "#FEC006", 1775, 0.18f, 156);
    ChartCircle action9 = new ChartCircle("DT hòa mạng TS và DVSB", "#2F4F4F", 1570f, 0.16f, 155);

    actionList.add(action);
    actionList.add(action1);
    actionList.add(action2);
    actionList.add(action3);
    actionList.add(action4);
    actionList.add(action5);
    actionList.add(action6);
    actionList.add(action7);
    actionList.add(action8);
    actionList.add(action9);

    return message;
  }

  public static Message getTextMessage() {
    return getTextMessage(getRandomMessage());
  }

  public static Message getTextMessage(String text) {
    return new Message(getRandomId(), getUser(), text);
  }

  public static ArrayList<Message> getMessages(Date startDate) {
    ArrayList<Message> messages = new ArrayList<>();
    for (int i = 0; i < 10/*days count*/; i++) {
      int countPerDay = rnd.nextInt(5) + 1;

      for (int j = 0; j < countPerDay; j++) {
        Message message;
        if (i % 2 == 0 && j % 3 == 0) {
          message = getImageMessage();
        } else {
          message = getTextMessage();
        }

        Calendar calendar = Calendar.getInstance();
        if (startDate != null) calendar.setTime(startDate);
        calendar.add(Calendar.DAY_OF_MONTH, -(i * i + 1));

        message.setCreatedAt(calendar.getTime());
        messages.add(message);
      }
    }
    return messages;
  }

  private static User getUser() {
    boolean even = rnd.nextBoolean();
    return new User(
        even ? "0" : "1",
        even ? names.get(0) : names.get(1),
        even ? avatars.get(0) : avatars.get(1),
        true);
  }

  public static User getMyUser() {
    return new User("0", names.get(0), avatars.get(0), true);
  }

  public static User getBotUser() {
    return new User("1", names.get(1), avatars.get(1), true);
  }
}
