package com.vtt.chatlib.chatdetail.data.model;

import com.google.gson.annotations.SerializedName;
import com.vtt.chatlib.chatkit.models.IMessage;
import com.vtt.chatlib.chatkit.models.MessageContentType;
import com.vtt.chatlib.mbi.dto.blockInfo.InfoBlock;
import com.vtt.chatlib.mbi.dto.horizontalChart.ItemHorizontalStackRankingUnitData;
import com.vtt.chatlib.mbi.dto.lineChart.ItemLineChartServiceDTO;

import java.util.ArrayList;
import java.util.Date;

/*
 * Created by troy379 on 04.04.17.
 */
public class Message implements IMessage,
    MessageContentType.Image, /*this is for default image messages implementation*/
    MessageContentType, /*and this one is for custom content type (in this case - voice message)*/
    MessageContentType.Link,
    MessageContentType.QuickReply,
    MessageContentType.ActionList,
    MessageContentType.ImageLoading,
    MessageContentType.BlockInfo,
    MessageContentType.LineChart,
    MessageContentType.HorizontalRankChart,
    MessageContentType.AvailableFeatureType,
    MessageContentType.ConclusionType,
    MessageContentType.RankingInfoType {

  private String id;
  private String text;
  private String subText;
  private Date createdAt;
  private User user;
  private Image image;
  private Voice voice;
  private Link messageLink;
  private QuickReply messageAction;
  private Button messageButton;
  private Image imageLoading;

  private InfoBlock blockInfo;
  private ItemLineChartServiceDTO itemLineChart;
  private ItemHorizontalStackRankingUnitData itemHorizontalChart;
  private ArrayList<AvailableFeature> availableFeatures;
  private ConclusionMessage conclusion;
  private Object payload;
  private InfoBlock rankingInfo;

  public Message(String id, User user, String text) {
    this(id, user, text, new Date());
  }

  public Message(String id, User user, String text, Date createdAt) {
    this.id = id;
    this.text = text;
    this.user = user;
    this.createdAt = createdAt;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public String getText() {
    return text;
  }

  @Override
  public String getSubText() {
    return subText;
  }

  @Override
  public Date getCreatedAt() {
    return createdAt;
  }

  @Override
  public User getUser() {
    return this.user;
  }

  @Override
  public String getImageUrl() {
    return image == null ? null : image.url;
  }

  @Override
  public String getLinkUrl() {
    return messageLink == null ? null : messageLink.linkUrl;
  }

  @Override
  public String getLinkDescription() {
    return messageLink == null ? null : messageLink.description;
  }

  @Override
  public String getLinkImage() {
    return messageLink == null ? null : messageLink.imageUrl;
  }

  public Link getMessageLink() {
    return messageLink;
  }

  public void setMessageLink(Link messageLink) {
    this.messageLink = messageLink;
  }

  public QuickReply getMessageAction() {
    return messageAction;
  }

  public void setMessageAction(QuickReply messageAction) {
    this.messageAction = messageAction;
  }

  public Button getMessageButton() {
    return messageButton;
  }

  public void setMessageButton(Button messageButton) {
    this.messageButton = messageButton;
  }

  public Voice getVoice() {
    return voice;
  }

  public String getStatus() {
    return "Sent";
  }

  public void setText(String text) {
    this.text = text;
  }

  public void setSubText(String subText) {
    this.subText = subText;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public void setImage(Image image) {
    this.image = image;
  }

  public void setImageLoading(Image imageLoading) {
    this.imageLoading = imageLoading;
  }

  public void setVoice(Voice voice) {
    this.voice = voice;
  }

  @Override
  public ArrayList<com.vtt.chatlib.chatkit.models.QuickReply> getActionList() {
    return messageAction == null ? null : messageAction.actionList;
  }

  @Override
  public ArrayList<com.vtt.chatlib.chatkit.models.Button> getButtonList() {
    return messageButton == null ? null : messageButton.buttonList;
  }

  @Override
  public String getImageResource() {
    return imageLoading == null ? null : imageLoading.url;
  }

  public void setBlockInfo(InfoBlock blockInfo) {
    this.blockInfo = blockInfo;
  }

  @Override
  public InfoBlock getInfoBlock() {
    return blockInfo;
  }

  public void setItemLineChart(ItemLineChartServiceDTO itemLineChart) {
    this.itemLineChart = itemLineChart;
  }

  @Override
  public ItemLineChartServiceDTO getItemLineChart() {
    return itemLineChart;
  }

  public void setItemHorizontalChart(ItemHorizontalStackRankingUnitData itemHorizontalChart) {
    this.itemHorizontalChart = itemHorizontalChart;
  }

  @Override
  public ItemHorizontalStackRankingUnitData getItemHorizontalStackRankingChart() {
    return itemHorizontalChart;
  }

  public void setAvailableFeatures(ArrayList<com.vtt.chatlib.chatdetail.data.model.AvailableFeature> availableFeatures) {
    this.availableFeatures = availableFeatures;
  }

  @Override
  public ArrayList<com.vtt.chatlib.chatdetail.data.model.AvailableFeature> getAvailableFeatures() {
    return availableFeatures == null ? null : availableFeatures;
  }

  public Object getPayload() {
    return payload;
  }

  public void setPayload(Object payload) {
    this.payload = payload;
  }

  public void setConclusion(ConclusionMessage conclusion) {
    this.conclusion = conclusion;
  }

  @Override
  public ConclusionMessage getConclusionMessage() {
    return conclusion;
  }

  @Override
  public InfoBlock getRankingInfo() {
    return rankingInfo;
  }

  public void setRankingInfo(InfoBlock rankingInfo) {
    this.rankingInfo = rankingInfo;
  }

  public static class Image {

    private String url;

    public Image(String url) {
      this.url = url;
    }
  }

  public static class QuickReply {

    @SerializedName("actionList")
    private ArrayList<com.vtt.chatlib.chatkit.models.QuickReply> actionList;

    public QuickReply(ArrayList<com.vtt.chatlib.chatkit.models.QuickReply> actionList) {
      this.actionList = actionList;
    }

    public ArrayList<com.vtt.chatlib.chatkit.models.QuickReply> getActionList() {
      return actionList;
    }
  }

  public static class Button {

    private ArrayList<com.vtt.chatlib.chatkit.models.Button> buttonList;

    public Button(ArrayList<com.vtt.chatlib.chatkit.models.Button> buttonList) {
      this.buttonList = buttonList;
    }
  }

  public static class Chart {

    private String header;
    private ArrayList<com.vtt.chatlib.chatkit.models.ChartCircle> chartArrayList;

    public Chart(ArrayList<com.vtt.chatlib.chatkit.models.ChartCircle> chartArrayList, String header) {
      this.chartArrayList = chartArrayList;
      this.header = header;
    }
  }

  public static class Link {

    private String imageUrl;
    private String linkUrl;
    private String description;

    public Link(String url, String link, String description) {
      this.imageUrl = url;
      this.linkUrl = link;
      this.description = description;
    }
  }

  public static class Voice {

    private String url;
    private int duration;

    public Voice(String url, int duration) {
      this.url = url;
      this.duration = duration;
    }

    public String getUrl() {
      return url;
    }

    public int getDuration() {
      return duration;
    }
  }
}
