package com.vtt.chatlib.chatdetail.data.model;

/**
 * Created by Hungpq on 1/3/2018.
 */

public class ConclusionMessage {

  private Object payload;
  private String title;
  private String description;
  private String icon;
  private String footer;

  public Object getPayload() {
    return payload;
  }

  public void setPayload(Object payload) {
    this.payload = payload;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public String getFooter() {
    return footer;
  }

  public void setFooter(String footer) {
    this.footer = footer;
  }
}
