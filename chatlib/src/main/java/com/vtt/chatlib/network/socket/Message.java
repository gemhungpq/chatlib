package com.vtt.chatlib.network.socket;

import com.google.gson.JsonElement;
import com.vtt.chatlib.network.socket.model.MessageData;

/**
 * Created by Macbook on 3/16/17.
 */

public class Message {
  private String service;
  private MessageData data;
  private JsonElement type;
  //  private AppConfig params;
  private String result;
  private String id;
  private String token;

  public String getToken() {
    return token;
  }

  public Message setToken(String token) {
    this.token = token;
    return this;
  }

  public String getService() {
    return service;
  }

  public Message setService(String service) {
    this.service = service;
    return this;
  }

  public MessageData getData() {
    return data;
  }

  public Message setData(MessageData data) {
    this.data = data;
    return this;
  }

  public JsonElement getType() {
    return type;
  }

  public Message setType(JsonElement type) {
    this.type = type;
    return this;
  }

//  public AppConfig getParams() {
//    return params;
//  }
//
//  public Message setParams(AppConfig params) {
//    this.params = params;
//    return this;
//  }

  public String getResult() {
    return result;
  }

  public Message setResult(String result) {
    this.result = result;
    return this;
  }

  public String getId() {
    return id;
  }

  public Message setId(String id) {
    this.id = id;
    return this;
  }
}
