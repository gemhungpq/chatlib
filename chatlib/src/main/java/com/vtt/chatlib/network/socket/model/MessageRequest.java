package com.vtt.chatlib.network.socket.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by HaiKE on 10/18/17.
 */

public class MessageRequest implements Serializable {

  @SerializedName("text")
  @Expose
  private String text;
  @SerializedName("payload")
  @Expose
  private String payload;
  @SerializedName("timestamp")
  @Expose
  private long timestamp;
  @SerializedName("mid")
  @Expose
  private String mid;
  private final static long serialVersionUID = 2362148289145969098L;

  public MessageRequest(String text, String payload, long timestamp, String mid) {
    this.text = text;
    this.payload = payload;
    this.timestamp = timestamp;
    this.mid = mid;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getPayload() {
    return payload;
  }

  public void setPayload(String payload) {
    this.payload = payload;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  public String getMid() {
    return mid;
  }

  public void setMid(String mid) {
    this.mid = mid;
  }

  @Override
  public String toString() {
    return "Message{" +
        "text='" + text + '\'' +
        ", payload='" + payload + '\'' +
        ", timestamp=" + timestamp +
        ", mid='" + mid + '\'' +
        '}';
  }
}
