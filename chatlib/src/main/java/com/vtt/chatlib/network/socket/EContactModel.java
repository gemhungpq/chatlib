package com.vtt.chatlib.network.socket;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HaiKE on 10/22/17.
 */

public class EContactModel {
  @SerializedName("phoneNumber")
  private String phoneNumber;

  @SerializedName("domain")
  private String domain;

  @SerializedName("body")
  private String body;

  @SerializedName("id")
  private String id;

  @SerializedName("from")
  private String from;

  @SerializedName("picture")
  private String picture;

  @SerializedName("message")
  private String message;

  public EContactModel(String phoneNumber, String domain, String body, String id, String from, String picture, String message) {
    this.phoneNumber = phoneNumber;
    this.domain = domain;
    this.body = body;
    this.id = id;
    this.from = from;
    this.picture = picture;
    this.message = message;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getDomain() {
    return domain;
  }

  public void setDomain(String domain) {
    this.domain = domain;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
