package com.vtt.chatlib.utils;

/**
 * Created by HaiKE on 10/18/17.
 */

public class Constants {
  public static final String DOMAIN = "mbi";
  public static final String TYPE_TEXT = "text";
  public static final String TYPE_METADATA = "metadata";
  public static final String TYPE_LINK = "link";
  public static final String TYPE_IMAGE = "image";
  public static final String TYPE_ACTION = "action";

  public static final String TYPE_ACTION_CARD = "action_card";
  public static final String TYPE_ACTION_REDIRECT = "action_redirect";

  public static final String KEY_ACTION_SERVER = "server";
  public static final String KEY_ACTION_APP = "app";
  public static final String TYPE_QUICK_ACTION_TEXT = "text";
  public static final String TYPE_QUICK_ACTION_LIKE = "like";
  public static final String TYPE_QUICK_ACTION_DISLIKE = "dislike";
}
