package com.vtt.chatlib.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Hungpq on 12/13/2017.
 */

public class DateTimeUtils {
  /**
   * With format dd/MM/yyyy
   */
  public static Date parse(String dateString) {
    final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    try {
      return DATE_FORMAT.parse(dateString);
    } catch (Exception e) {
      Logger.log(e);
    }

    return null;
  }
}
