package com.vtt.chatlib.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vtt.chatlib.R;
import com.vtt.chatlib.chatheads.ChatHeadUtils;

public class ProportionView extends FrameLayout {

  private static final float EPSILON = 0.00000001f;

  String mShowText;
  float mPercentComplete;
  int mColor;
  float mTextSize;

  View mParentV;
  View mTotalV;
  View mCompleteV;
  View mLine100V;

  boolean mIsProgressStyle;

  TextView mValueTv;
  RelativeLayout.LayoutParams match_parent = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
      ViewGroup.LayoutParams.MATCH_PARENT);
  RelativeLayout.LayoutParams custom_size = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
      ViewGroup.LayoutParams.MATCH_PARENT);
  RelativeLayout.LayoutParams line100_layout = new RelativeLayout.LayoutParams(ChatHeadUtils.dpToPx(getContext(), 2),
      ViewGroup.LayoutParams.MATCH_PARENT);

  public ProportionView(Context context) {
    super(context);
    init(context, null);
  }

  public ProportionView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init(context, attrs);
  }

  public ProportionView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs);
  }

  private void init(Context context, AttributeSet attrs) {
    custom_size.setMargins(0, 0, 0, 0);
    TypedArray a = context.getTheme().obtainStyledAttributes(
        attrs,
        R.styleable.ProportionViewChatBot,
        0, 0);

    try {
      mShowText = a.getString(R.styleable.ProportionViewChatBot_text);
      mPercentComplete = a.getFloat(R.styleable.ProportionViewChatBot_percentComplete, 0f);
      mColor = a.getColor(R.styleable.ProportionViewChatBot_colorComplete, Color.BLACK);
      mTextSize = a.getFloat(R.styleable.ProportionViewChatBot_textSizeProportion, 0);
      mIsProgressStyle = a.getBoolean(R.styleable.ProportionViewChatBot_isProgressStyle, false);
    } finally {
      a.recycle();
    }
    mIsProgressStyle = false;

    inflate(getContext(), R.layout.proportion_view, this);
    mParentV = findViewById(R.id.parentView);
    mTotalV = findViewById(R.id.totalV);
    mCompleteV = findViewById(R.id.completeV);
    mValueTv = (TextView) findViewById(R.id.valueTv);
    mLine100V = findViewById(R.id.line100v);

    mCompleteV.setBackgroundColor(mColor);
    if (mShowText != null) {
      mValueTv.setText(mShowText);
    }

    mValueTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, mTextSize);
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
    if (mPercentComplete > 100f) {
      //Complete > 100%
      int w = (int) (parentWidth * 100 / mPercentComplete);
      if (mIsProgressStyle) {
        custom_size.setMargins(0, ChatHeadUtils.dpToPx(getContext(), 2), 0, ChatHeadUtils.dpToPx(getContext(), 2));
        match_parent.setMargins(0, ChatHeadUtils.dpToPx(getContext(), 2), 0, ChatHeadUtils.dpToPx(getContext(), 2));
      } else {
        custom_size.setMargins(0, 0, 0, 0);
        match_parent.setMargins(0, 0, 0, 0);
      }
      line100_layout.setMargins(w, 0, 0, 0);
      mLine100V.setVisibility(VISIBLE);
      mLine100V.setLayoutParams(line100_layout);
      if (mIsProgressStyle) {
        mLine100V.setBackgroundColor(Color.BLACK);
        mLine100V.setAlpha(1);
      }
      mCompleteV.setLayoutParams(match_parent);
      mTotalV.setLayoutParams(match_parent);
    } else if (isZero(mPercentComplete - 100f)) {
      //Complete = 100%
      if (mIsProgressStyle) {
        custom_size.setMargins(0, ChatHeadUtils.dpToPx(getContext(), 2), 0, ChatHeadUtils.dpToPx(getContext(), 2));
      } else {
        custom_size.setMargins(0, 0, 0, 0);
      }
      mLine100V.setVisibility(GONE);
      mCompleteV.setLayoutParams(custom_size);
      mTotalV.setLayoutParams(custom_size);
    } else {
      mLine100V.setVisibility(GONE);
      //Complete < 100%
      custom_size.width = (int) (parentWidth * mPercentComplete / 100);
      if (mIsProgressStyle) {
        custom_size.setMargins(0, ChatHeadUtils.dpToPx(getContext(), 2), 0, ChatHeadUtils.dpToPx(getContext(), 2));
        match_parent.setMargins(0, ChatHeadUtils.dpToPx(getContext(), 2), 0, ChatHeadUtils.dpToPx(getContext(), 2));
      } else {
        custom_size.setMargins(0, 0, 0, 0);
        match_parent.setMargins(0, 0, 0, 0);
      }
      mCompleteV.setLayoutParams(custom_size);
      mTotalV.setLayoutParams(match_parent);
    }
    mCompleteV.setBackgroundColor(mColor);
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
  }

  public void setPercentComplete(Float percentComplete) {
    if (percentComplete == null) mPercentComplete = 0f;
    else mPercentComplete = percentComplete;
    refresh();
  }

  public void setTextSize(int idTextSize) {
    mValueTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, idTextSize);
    refresh();
  }

  public void setText(String text) {
    mValueTv.setText(text);
    refresh();
  }

  public void hideTextValue() {
    mValueTv.setVisibility(GONE);
  }

  public void setColor(int color) {
    mColor = color;
    refresh();
  }

  public void setLineColor(int color) {
    mLine100V.setBackgroundColor(color);
    refresh();
  }

  private static boolean isZero(float num) {
    return num >= -EPSILON && num <= EPSILON;
  }

  public void refresh() {
    requestLayout();
    invalidate();
  }

  public void setIsProgressStyle(boolean mIsProgressStyle) {
    this.mIsProgressStyle = mIsProgressStyle;
    refresh();
  }
}
