package com.vtt.chatlib.utils;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.DimenRes;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.vtt.chatlib.BuildConfig;

import java.util.Random;

public class Utils {
  public static final String LogTag = "henrytest";
  public static final String EXTRA_MSG = "extra_msg";

  public static boolean isKeyBoardShow = false;

  public static boolean canDrawOverlays(Context context) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
      return true;
    } else {
      return Settings.canDrawOverlays(context);
    }
  }

  public static String generateToken() {
    //timestamp + os + version app + username + random number
    Random random = new Random();
    int ran = random.nextInt(9999);
//		return System.currentTimeMillis() + "android_" + Build.VERSION.RELEASE + BuildConfig.VERSION_CODE + ran;
    return System.currentTimeMillis() + "android_" + BuildConfig.VERSION_CODE + ran;
  }

  public static void copyToClipboard(Context context, String copiedText) {
    if (copiedText != null && !copiedText.isEmpty()) {
      ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
      ClipData clip = ClipData.newPlainText(copiedText, copiedText);
      clipboard.setPrimaryClip(clip);
    }
  }

  public static void hideKeyboard(Context context) {
    InputMethodManager inputManager = (InputMethodManager) context.
        getSystemService(Context.INPUT_METHOD_SERVICE);
    if (inputManager != null) {
      inputManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
  }

  public static void hideKeyboard(Context context, View view) {
    InputMethodManager inputManager = (InputMethodManager) context.
        getSystemService(Context.INPUT_METHOD_SERVICE);
    if (inputManager != null) {
      inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
  }

  public static GradientDrawable getGradientDrawable(int color) {
    int roundRadius = 15; // 8px not dp
    GradientDrawable gd = new GradientDrawable();
    gd.setColor(color);
    gd.setCornerRadius(roundRadius);
    return gd;
  }

  public static int getValueById(Context context, @DimenRes int dimenId) {
    return (int) context.getResources().getDimension(dimenId);
  }
}
