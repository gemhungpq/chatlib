package com.vtt.chatlib.mbi.dto.common;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.vtt.chatlib.R;
import com.vtt.chatlib.mbi.dto.lineChart.LChartLine;
import com.vtt.chatlib.mbi.dto.lineChart.LChartLinePoint;
import com.vtt.chatlib.mbi.uicomponent.MyMarkerView;
import com.vtt.chatlib.utils.DateTimeUtils;
import com.vtt.chatlib.utils.RecyclerUtils;
import com.vtt.chatlib.utils.StringUtils;

import org.apache.commons.lang3.time.DateUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by Macbook on 2/27/17.
 */

public class Charts {

  public static void initLineChart(Context context, final LineChart mLineChart) {
    mLineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
      @Override
      public void onValueSelected(Entry e, Highlight h) {
        mLineChart.centerViewToAnimated(e.getX(), e.getY(), mLineChart.getData().getDataSetByIndex(h.getDataSetIndex())
            .getAxisDependency(), 500);
      }

      @Override
      public void onNothingSelected() {

      }
    });

    mLineChart.setDrawGridBackground(false);
    mLineChart.setDrawBorders(false);

    mLineChart.getAxisLeft().setEnabled(true);
    mLineChart.getAxisRight().setEnabled(false);
    mLineChart.getAxisRight().setDrawAxisLine(false);
    mLineChart.getAxisRight().setDrawGridLines(false);
    mLineChart.getAxisLeft().setDrawGridLines(false);

    mLineChart.getAxisLeft().setValueFormatter(new GemLargeValueFormatter());
    mLineChart.setDoubleTapToZoomEnabled(false);

    XAxis xAxis = mLineChart.getXAxis();
    xAxis.setEnabled(true);
    xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
    xAxis.setLabelRotationAngle(270);
    xAxis.setDrawAxisLine(true);
    xAxis.setDrawGridLines(false);
    xAxis.setGranularityEnabled(true);
    xAxis.setGranularity(1.0f);
    xAxis.setAxisMinimum(0);
    xAxis.setSpaceMax(1.0f);

    YAxis yAxis = mLineChart.getAxisLeft();
    yAxis.setDrawGridLines(true);
    yAxis.setGridColor(ContextCompat.getColor(context, R.color.grid_chart));
    yAxis.setDrawZeroLine(true);
    // enable touch gestures
    mLineChart.setTouchEnabled(true);

    // enable scaling and dragging
    mLineChart.setDragEnabled(false);
    mLineChart.setScaleEnabled(false);
    // if disabled, scaling can be done on x- and y-axis separately
    mLineChart.setPinchZoom(false);

    mLineChart.getDescription().setEnabled(false);
    mLineChart.getDescription().setText("");
    mLineChart.setNoDataText(context.getString(R.string.no_data_chart));
    mLineChart.invalidate();


    // create a custom MarkerView (extend MarkerView) and specify the layout
    // to use for it
    MyMarkerView mv = new MyMarkerView(context, R.layout.custom_marker_view_chat);
    mv.setChartView(mLineChart); // For bounds control

// Set the marker to the chart
    mLineChart.setMarker(mv);

    // Legend
    Legend legend = mLineChart.getLegend();
    legend.setEnabled(false);
    mLineChart.invalidate();
  }

  public static void configureChartYAxisLabels(BarLineChartBase chart, List<String> dates) {
    IAxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(chart, dates);

    XAxis xAxis = chart.getXAxis();
    xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//    xAxis.setTypeface(mTfLight);
    xAxis.setDrawGridLines(false);
    xAxis.setGranularity(1f); // only intervals of 1 day
    xAxis.setLabelCount(dates.size());
    xAxis.setValueFormatter(xAxisFormatter);
  }

  public static Map<String, Integer> getDateMapLine(List<LChartLine> LChartLines, List<String> dateListOutPut) {
    Map<String, Integer> datePointMap = new HashMap<>();
    List<String> dateStrings = new ArrayList<>();

    for (LChartLine chartLine : LChartLines) {
      if (chartLine != null) {
        List<LChartLinePoint> points = chartLine.getPoints();
        if (points != null && !points.isEmpty()) {
          for (LChartLinePoint point : points) {
            if (point == null) continue;
            String dateString = point.getDate();

//            if (null == dateString) dateString = "Chưa xác định";
            if (null == dateString) continue;

            // Check if isn't add to map
            if (!datePointMap.containsKey(dateString)) {
              datePointMap.put(dateString, null);
              dateStrings.add(dateString);
            }
          }
        }
      }
    }

    // Re-order list
    Collections.sort(dateStrings, new Comparator<String>() {
      @Override
      public int compare(String o1, String o2) {
        Date date1 = DateTimeUtils.parse(o1);
        Date date2 = DateTimeUtils.parse(o2);

        if (date1 == null || date2 == null) {
          return 0;
        } else {
          if (DateUtils.isSameDay(date1, date2)) {
            return 0;
          } else if (date1.before(date2)) {
            return -1;
          } else {
            return 1;
          }
        }

//        return (int) (date1.getTimeAbsolute() - date2.getTimeAbsolute());
      }
    });

    // Add x Axis value to map
    dateListOutPut.addAll(dateStrings);
    for (int i = 0; i < dateStrings.size(); i++) {
      datePointMap.put(dateStrings.get(i), i);
    }

    return datePointMap;
  }

  public static void initHorizontalTopchart(Context context, BarChart mBarChart, float minX, float maxX) {
    mBarChart.setDrawGridBackground(false);
    mBarChart.setPinchZoom(false);
    mBarChart.setDoubleTapToZoomEnabled(false);
    mBarChart.setScaleEnabled(false);
    mBarChart.setHighlightPerDragEnabled(false);
    mBarChart.setDrawBarShadow(false);
    mBarChart.setHighlightFullBarEnabled(false);
    mBarChart.getDescription().setEnabled(false);
    mBarChart.getRendererXAxis().getPaintAxisLabels().setTextAlign(Paint.Align.CENTER);
    mBarChart.getLegend().setEnabled(false);
    mBarChart.setDrawValueAboveBar(false);
    mBarChart.setHighlightPerTapEnabled(false);
    // x axis
    XAxis xAxis = mBarChart.getXAxis();
    xAxis.setDrawLabels(true);
    xAxis.setGridColor(ContextCompat.getColor(context, R.color.grid_chart));

//    xAxis.setGranularityEnabled(true);
    xAxis.setAxisMinimum(-0.8f);
    xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
    xAxis.setDrawGridLines(false);
//    xAxis.setCenterAxisLabels(true);

    YAxis leftAxis = mBarChart.getAxisLeft();
    leftAxis.setDrawGridLines(false);
    leftAxis.setEnabled(false);
    YAxis rightAxis = mBarChart.getAxisRight();
    rightAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
    rightAxis.setDrawGridLines(true);
    rightAxis.setDrawAxisLine(false);

    rightAxis.setEnabled(true);
    rightAxis.setValueFormatter(new GemLargeValueFormatter());

  }

  public static void setupLegends(RecyclerView mLegendRv,
                                  List<ItemLegend> listLegends,
                                  LegendsAdapter.OnItemLegendClickListener listener) {
    RecyclerUtils.setupVerticalRecyclerView(mLegendRv.getContext(), mLegendRv);
    LegendsAdapter mAdapter = new LegendsAdapter(listLegends, false);
    mAdapter.setOnItemLegendClickListener(listener);
    mLegendRv.setAdapter(mAdapter);
  }

  /**
   * Default colors
   **/
  protected static final String[] Colors = new String[]{"#00FF00", "#0000FF", "#FF0000", "#00FFFF"};

  public static int getColor(String color, int position) {
    if (StringUtils.isEmpty(color)) {
      return Color.parseColor(Colors[position % Colors.length]);
    }

    try {
      return Color.parseColor(color);
    } catch (IllegalArgumentException ex) {
      com.vtt.chatlib.utils.Logger.log(ex);
      return Color.parseColor(Colors[position % Colors.length]);
    }
  }
}

