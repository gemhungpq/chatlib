package com.vtt.chatlib.mbi.dto.blockInfo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by hungl on 5/24/2017.
 */

public class InfoBlock {
  @SerializedName("reportName")
  private String reportName;
  @SerializedName("unit")
  private String unit;
  @SerializedName("mainBlock")
  private MainBlock mainBlock;
  @SerializedName("otherInfo")
  private List<SubBlock> otherInfo;
  @SerializedName("rankingBlock")
  private List<RankingInfoMainBlock> rankingBlock;

  public String getReportName() {
    if (reportName == null || "".equals(reportName)) {
      String rs = "";
      return rs;
    } else {
      return reportName;
    }
  }

  public InfoBlock setReportName(String reportName) {
    this.reportName = reportName;
    return this;
  }

  public String getUnit() {
    return unit;
  }

  public InfoBlock setUnit(String unit) {
    this.unit = unit;
    return this;
  }

  public MainBlock getMainBlock() {
    return mainBlock;
  }

  public InfoBlock setMainBlock(MainBlock mainBlock) {
    this.mainBlock = mainBlock;
    return this;
  }

  public List<SubBlock> getOtherInfo() {
    return otherInfo;
  }

  public void setOtherInfo(List<SubBlock> otherInfo) {
    this.otherInfo = otherInfo;
  }

  public List<RankingInfoMainBlock> getRankingBlock() {
    return rankingBlock;
  }

  public void setRankingBlock(List<RankingInfoMainBlock> rankingBlock) {
    this.rankingBlock = rankingBlock;
  }

  public static class RankingInfoMainBlock {
    String currentRank;
    String unit;
    String lastRank;
    String deltaRank;
    String level;

    public String getCurrentRank() {
      return currentRank;
    }

    public void setCurrentRank(String currentRank) {
      this.currentRank = currentRank;
    }

    public String getUnit() {
      return unit;
    }

    public void setUnit(String unit) {
      this.unit = unit;
    }

    public String getLastRank() {
      return lastRank;
    }

    public void setLastRank(String lastRank) {
      this.lastRank = lastRank;
    }

    public String getDeltaRank() {
      return deltaRank;
    }

    public void setDeltaRank(String deltaRank) {
      this.deltaRank = deltaRank;
    }

    public String getLevel() {
      return level;
    }

    public void setLevel(String level) {
      this.level = level;
    }
  }
}
