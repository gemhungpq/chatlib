package com.vtt.chatlib.mbi.dto.lineChart;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * AccDayChart DTO
 * Created by NEO on 2/22/2017.
 */
public class LChart {
  @SerializedName("chartTitle")
  private String chartTitle;

  @SerializedName("unit")
  private String valueUnit;

  @SerializedName("lines")
  private List<LChartLine> lines;

  @SerializedName("avglines")
  private List<LChartLine> avgLines;


  public String getChartTitle() {
    return chartTitle;
  }

  public LChart setChartTitle(String chartTitle) {
    this.chartTitle = chartTitle;
    return this;
  }

  public String getValueUnit() {
    return valueUnit;
  }

  public LChart setValueUnit(String valueUnit) {
    this.valueUnit = valueUnit;
    return this;
  }

  public List<LChartLine> getLines() {
    return lines;
  }

  public LChart setLines(List<LChartLine> lines) {
    this.lines = lines;
    return this;
  }

  public List<LChartLine> getAvgLines() {
    return avgLines;
  }

  public LChart setAvgLines(List<LChartLine> avgLines) {
    this.avgLines = avgLines;
    return this;
  }

  public List<LChartLine> getAllLines() {
    List<LChartLine> lines = new ArrayList<>();
    if (this.lines != null)
      lines.addAll(this.lines);
    if (avgLines != null)
      lines.addAll(avgLines);
    return lines;
  }
}
