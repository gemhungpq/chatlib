package com.vtt.chatlib.mbi.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 4/6/2017.
 */

public class BaseItemService {
  String id;
  @SerializedName("blockId")
  String blockId;
  String type;
  Object data;

  public String getId() {
    return id;
  }

  public BaseItemService setId(String id) {
    this.id = id;
    return this;
  }

  public String getBlockId() {
    return blockId;
  }

  public BaseItemService setBlockId(String blockId) {
    this.blockId = blockId;
    return this;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }
}
