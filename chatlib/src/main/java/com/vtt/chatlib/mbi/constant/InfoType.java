package com.vtt.chatlib.mbi.constant;

/**
 * Created by hungl on 5/25/2017.
 */

public enum InfoType {
  PERFORM_INFO,
  DELTA_INFO
}
