package com.vtt.chatlib.mbi.dto.common;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vtt.chatlib.R;
import com.vtt.chatlib.mbi.constant.InfoType;
import com.vtt.chatlib.mbi.dto.blockInfo.SubBlock;
import com.vtt.chatlib.mbi.dto.blockInfo.SubInfo;
import com.vtt.chatlib.utils.NumberUtils;
import com.vtt.chatlib.utils.ProportionView;
import com.vtt.chatlib.utils.StringUtils;

import java.util.List;


/**
 * Created by hungpq on 5/25/2017.
 */

public class AreaDetailsAdapter extends RecyclerView.Adapter<AreaDetailsAdapter.AreaDetailsVH> {
  private List<SubBlock> mListSubBlock;
  private boolean isDetailScreen = false;

  public AreaDetailsAdapter(List<SubBlock> otherInfo) {
    mListSubBlock = otherInfo;
    isDetailScreen = false;
  }

  public AreaDetailsAdapter setDetailScreen() {
    isDetailScreen = true;
    return this;
  }

  @Override
  public int getItemViewType(int position) {
    return InfoType.valueOf(mListSubBlock.get(position).getType()).ordinal();
  }

  @Override
  public AreaDetailsVH onCreateViewHolder(ViewGroup parent, int viewType) {
    return new AreaDetailsVH(LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_area_details_map_chat_bot, parent, false));
  }

  @Override
  public void onBindViewHolder(AreaDetailsVH holder, int position) {
    SubInfo info = mListSubBlock.get(position).getData();
    holder.performPv.setIsProgressStyle(true);
    holder.performPv.setLineColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.black));
    holder.performPv.hideTextValue();
    holder.nameItemTv.setText(StringUtils.checkNullString(info.getTitle()));
    if (getItemViewType(position) == InfoType.PERFORM_INFO.ordinal()) {
      String valueView = StringUtils.checkNullString(info.getPerformValue());
      if (info.getPerformPercent() != null)
        valueView += " (" + NumberUtils.formatPercent(info.getPerformPercent()) + ")";
      holder.valueItemTv.setText(valueView);
      holder.deltaTv.setVisibility(View.GONE);
      if (info.getPlanValue() != null || info.getPerformPercent() != null) {
        holder.performPv.setVisibility(View.VISIBLE);
        holder.performPv.setColor(Color.parseColor(info.getPerformColor()));
        // TODO: margin progressbar in detail screen
//        if (isDetailScreen) {
//          LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//              (int) DimensionUtils.dpToPx(holder.itemView.getContext(), 22));
//          layoutParams.setMargins(0, 0,
//              (int) DimensionUtils.dpToPx(holder.itemView.getContext(), 54), (int) DimensionUtils.dpToPx(holder.itemView.getContext(), 8));
//          holder.performPv.setLayoutParams(layoutParams);
//        }
        String proportionText = "";
        if (info.getPlanValue() == null || "".equals(info.getPlanValue())) {
          proportionText += NumberUtils.formatPercentWithCheckEmpty(holder.itemView.getContext(), info.getPerformPercent());
        } else {
          proportionText += info.getPlanValue();
          if (info.getPerformPercent() != null) {
            proportionText += " (" + NumberUtils.formatPercentWithCheckEmpty(holder.itemView.getContext(), info.getPerformPercent()) + ")";
          }
        }

//        holder.valueItemTv.setText(proportionText);
        holder.performPv.setText(proportionText);
        holder.performPv.setPercentComplete(info.getPerformPercent());
      } else {
        holder.performPv.setVisibility(View.INVISIBLE);
      }
    } else {
      holder.valueItemTv.setText(StringUtils.checkNullString(info.getPerformValue()));
      holder.deltaTv.setVisibility(View.VISIBLE);
      holder.performPv.setVisibility(View.GONE);

      String deltaText = "";
      if (info.getDeltaValue() == null) {
        deltaText += StringUtils.checkNullString(info.getDeltaPercent());
      } else {
        deltaText += info.getDeltaValue();
        if (info.getDeltaPercent() != null) {
          deltaText += " (" + info.getDeltaPercent() + ")";
        }
      }

      holder.deltaTv.setText(deltaText);
      holder.deltaTv.setTextColor(Color.parseColor(info.getDeltaColor()));

      if (info.getIncrease() == null) {
        holder.deltaTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
      } else if (info.getIncrease()) {
        holder.deltaTv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_triangle_up_green, 0, 0, 0);
      } else {
        holder.deltaTv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_triangle_down_red, 0, 0, 0);
      }
    }
  }

  @Override
  public int getItemCount() {
    return mListSubBlock.size();
  }

  class AreaDetailsVH extends RecyclerView.ViewHolder {
    TextView nameItemTv;
    TextView valueItemTv;
    ProportionView performPv;
    TextView deltaTv;

    AreaDetailsVH(View itemView) {
      super(itemView);
      nameItemTv = (TextView) itemView.findViewById(R.id.name_item_tv);
      valueItemTv = (TextView) itemView.findViewById(R.id.value_item_tv);
      performPv = (ProportionView) itemView.findViewById(R.id.perform_pv);
      deltaTv = (TextView) itemView.findViewById(R.id.delta_tv);

    }
  }
}
