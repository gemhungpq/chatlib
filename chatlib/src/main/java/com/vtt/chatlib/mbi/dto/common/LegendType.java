package com.vtt.chatlib.mbi.dto.common;

/**
 * Created by HoaPham on 5/18/17.
 */

public enum LegendType {
  GROUP_NAME, LINECHART, BARCHART, PIE_CHART, DASHED_LINE, LINE_NO_INDICATOR, DASHED_LINE_NO_INDICATOR
}
