package com.vtt.chatlib.mbi.dto.lineChart;

import android.graphics.Color;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.vtt.chatlib.mbi.dto.common.Charts;
import com.vtt.chatlib.mbi.dto.common.ItemLegend;
import com.vtt.chatlib.mbi.dto.common.LegendType;
import com.vtt.chatlib.mbi.dto.common.PopOverChartModel;
import com.vtt.chatlib.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by HoaPham on 4/25/17.
 */

public class ItemLineChartServiceDTO extends LChart implements IEntryPoupOverSearchable, Cloneable {
  //  @SerializedName("chartName")
//  String chartName;
//  @SerializedName("unit")
//  String unit;
//  @SerializedName("lines")
//  List<ItemOneLineChart> lines;
//  @SerializedName("filterMetadata")
//  private FilterMetadata filterMetadata;
  private LineData mLineData;
  private float maxValue;
  private List<ItemLegend> itemLegends;
  private List<String> dates = new ArrayList<>();
  private String objectType;

  private boolean isAdjusted;

//  public String getChartName() {
//    return chartName;
//  }
//
//  public void setChartName(String chartName) {
//    this.chartName = chartName;
//  }
//
//  public String getUnit() {
//    return unit;
//  }
//
//  public void setUnit(String unit) {
//    this.unit = unit;
//  }
//
//  public List<ItemOneLineChart> getLines() {
//    return lines;
//  }
//
//  public void setLines(List<ItemOneLineChart> lines) {
//    this.lines = lines;
//  }

  public LineData getLineData() {
    return mLineData;
  }

  public void updateChartData() {
//    List<LChartLine> avgLines = getAvgLines();
//    List<LChartLine> lines = getLines();
    List<LChartLine> lines = getAllLines();

    // update legends
    itemLegends = new ArrayList<>();
    if (lines != null && !lines.isEmpty()) {
      for (int i = 0; i < lines.size(); i++) {
        ItemLegend legend = new ItemLegend(lines.get(i).getTitle(),
            Color.parseColor(lines.get(i).getColor()), "", "");
        if (!lines.get(i).isAverage())
          legend.setLegendType(LegendType.LINECHART);
        else
          legend.setLegendType(LegendType.LINE_NO_INDICATOR);
//        legend.setLegendType(LegendType.LINECHART);
        itemLegends.add(legend);
      }
    }

    updateDates();
    updateLineData();
    updateMaxValue();
  }

  public void updateChartData(String[] serviceId) {
//    List<LChartLine> lines = getLines();
    List<LChartLine> lines = getAllLines();
    // update legends
    itemLegends = new ArrayList<>();
    if (lines != null && !lines.isEmpty()) {
      for (int i = 0; i < lines.size(); i++) {
        ItemLegend legend = new ItemLegend(lines.get(i).getTitle(),
            Color.parseColor(lines.get(i).getColor()), "", "");
        if (!lines.get(i).isAverage())
          legend.setLegendType(LegendType.LINECHART);
        else
          legend.setLegendType(LegendType.LINE_NO_INDICATOR);
        if (i < serviceId.length)
          legend.setObjectId(serviceId[i]);
        itemLegends.add(legend);
      }
    }

    updateDates();
    updateLineData();
    updateMaxValue();
  }

  private void updateDates() {
//    List<LChartLine> lines = getLines();
    List<LChartLine> lines = getAllLines();
    if (dates != null && !dates.isEmpty()) return;
    dates = new ArrayList<>();
    if (lines == null || lines.isEmpty()) {
      return;
    }

//    for (LChartLinePoint pointChart : lines.get(0).getPoints()) {
//      dates.add(pointChart.getDate());
//    }
    Charts.getDateMapLine(lines, dates);
  }

  private void updateLineData() {
    mLineData = new LineData();
    ArrayList<ILineDataSet> dataSets = new ArrayList<>();
//    List<LChartLine> lines = getLines();
    List<LChartLine> lines = getAllLines();

    if (lines != null && !lines.isEmpty()) {
      for (int z = 0; z < lines.size(); z++) {
        // Data for 1 line
        LChartLine chartLine = lines.get(z);
        if (null == chartLine) continue;

        ArrayList<Entry> values = new ArrayList<>();
        // Add points to line
        List<LChartLinePoint> points = chartLine.getPoints();
        if (points != null && !points.isEmpty()) {
          for (int i = 0; i < points.size(); i++) {
            LChartLinePoint point = points.get(i);
            if (point == null || null == point.getDate()) continue;
            if (i >= dates.size())
              break;
            if (!point.getDate().equalsIgnoreCase(dates.get(i))) {
              points.add(i, null);
              continue;
            }

            if (!StringUtils.isEmpty(point.getValue())) {
              values.add(new Entry(i, Float.valueOf(point.getValue()), point));
            }
          }
        }

        // Data set
        LineDataSet d = new LineDataSet(values, "DataSet" + (z + 1));

        d.setDrawValues(false);
        d.setLineWidth(1.5f);
        d.setCircleRadius(3f);
        d.setCircleHoleRadius(0f);
        if (lines.get(z).isAverage())
          d.enableDashedLine(10, 10, 0);
        if (!lines.get(z).isAverage())
          d.setDrawCircles(true);
        else
          d.setDrawCircles(false);
        d.setDrawCircleHole(false);
        d.setDrawHighlightIndicators(false);

        int color = Charts.getColor(chartLine.getColor(), z);

        d.setColor(color);
        d.setCircleColor(color);
        dataSets.add(d);
      }
    }

    mLineData = new LineData(dataSets);
  }

  private void updateMaxValue() {
//    List<LChartLine> lines = getLines();
    List<LChartLine> lines = getAllLines();

    if (lines == null) {
      maxValue = 0;
      return;
    }

    for (LChartLine itemOneLineChart : lines) {
      if (itemOneLineChart == null || itemOneLineChart.getPoints() == null
          || itemOneLineChart.getPoints().isEmpty()) {
        continue;
      }

      for (LChartLinePoint point : itemOneLineChart.getPoints()) {
        if (point == null) continue;
        float yVal = StringUtils.isEmpty(point.getValue()) ? 0 : Float.valueOf(point.getValue());
        if (maxValue < yVal) {
          maxValue = yVal;
        }
      }
    }
  }

  public float getMaxValue() {
    return maxValue;
  }

  public List<ItemLegend> getListLegends() {
    return itemLegends;
  }

  @Override
  public PopOverChartModel getPopOverModel(Entry entry) {
    if (mLineData == null) {
      return null;
    }

    ILineDataSet dataSet = mLineData.getDataSetForEntry(entry);
    if (dataSet == null) {
      return null;
    }

    int entryIndex = dataSet.getEntryIndex(entry);
    return getPopOverModel(entryIndex);
  }

  public PopOverChartModel getPopOverModel(int index) {
//    List<LChartLine> lines = getLines();
    List<LChartLine> lines = getAllLines();

    String valuePopOver = "";
    PopOverChartModel popOverChartModel = new PopOverChartModel();

    List<ItemLegend> itemLegends = new ArrayList<>();

    for (LChartLine itemOneLineChart : lines) {
      if (itemOneLineChart == null || itemOneLineChart.getPoints() == null || itemOneLineChart.getPoints().isEmpty())
        continue;

      ItemLegend i = null;

      if (index >= 0) {
//        LChartLinePoint point = itemOneLineChart.getPoints().get(index);
        if (index >= dates.size())
          index = dates.size() - 1;
        String date = dates.get(index);
        for (LChartLinePoint point : itemOneLineChart.getPoints()) {
          if (point == null || null == point.getDate()) {
            continue;
          }
          if (point.getDate().equals(date)) {
            String color = itemOneLineChart.getColor();
            String value = point.getViewValue();
            value = StringUtils.isEmpty(value) ? "N/A" : value;

            i = new ItemLegend(itemOneLineChart.getTitle(),
                null == color ? Color.BLACK : Color.parseColor(color),
                "", value);
            valuePopOver = point.getDate() + point.getEventText();
            break;
          }
        }

      } else {
        String color = itemOneLineChart.getColor();
        i = new ItemLegend(itemOneLineChart.getTitle(),
            null == color ? Color.BLACK : Color.parseColor(color), "", "");
      }

      if (i != null) {
        if (!itemOneLineChart.isAverage())
          i.setLegendType(LegendType.LINECHART);
        else
          i.setLegendType(LegendType.LINE_NO_INDICATOR);
        itemLegends.add(i);
      }
    }

    popOverChartModel.setTitle(getChartTitle()
        + (StringUtils.isEmpty(getValueUnit()) ? "" : (" (" + getValueUnit() + ")")));
    popOverChartModel.setColumnValue(valuePopOver);
    popOverChartModel.setItemLegends(itemLegends);
    popOverChartModel.setHidePercent(true);

    return popOverChartModel;
  }

  public List<String> getDates() {
    return dates;
  }

  public boolean isAdjusted() {
    return isAdjusted;
  }

  public ItemLineChartServiceDTO setAdjusted(boolean adjusted) {
    isAdjusted = adjusted;
    return this;
  }

  public void setDates(List<String> dates) {
    this.dates = dates;
  }

  public ItemLineChartServiceDTO setObjectType(String objectType) {
    this.objectType = objectType;
    return this;
  }

  public String getObjectType() {
    return objectType;
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }
}
