package com.vtt.chatlib.chatkit.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by HaiKE on 10/25/17.
 */

public class ChartArea implements Serializable {

  @SerializedName("blockId")
  @Expose
  private Integer blockId;
  @SerializedName("type")
  @Expose
  private String type;
  @SerializedName("data")
  @Expose
  private Data data;
  @SerializedName("id")
  @Expose
  private Integer id;
  private final static long serialVersionUID = -8986724527855507375L;

  public Integer getBlockId() {
    return blockId;
  }

  public void setBlockId(Integer blockId) {
    this.blockId = blockId;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "Example{" +
        "blockId=" + blockId +
        ", type='" + type + '\'' +
        ", data=" + data +
        ", id=" + id +
        '}';
  }

  public class Area implements Serializable {

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("event")
    @Expose
    private Object event;
    @SerializedName("areaPieces")
    @Expose
    private List<AreaPiece> areaPieces = null;
    private final static long serialVersionUID = 7206993793724157603L;

    public String getLabel() {
      return label;
    }

    public void setLabel(String label) {
      this.label = label;
    }

    public Object getEvent() {
      return event;
    }

    public void setEvent(Object event) {
      this.event = event;
    }

    public List<AreaPiece> getAreaPieces() {
      return areaPieces;
    }

    public void setAreaPieces(List<AreaPiece> areaPieces) {
      this.areaPieces = areaPieces;
    }

    @Override
    public String toString() {
      return "MessageChartArea{" +
          "label='" + label + '\'' +
          ", event=" + event +
          ", areaPieces=" + areaPieces +
          '}';
    }
  }

  public class AreaPiece implements Serializable {

    @SerializedName("drawValue")
    @Expose
    private Integer drawValue;
    @SerializedName("viewValue")
    @Expose
    private String viewValue;
    @SerializedName("styleId")
    @Expose
    private Integer styleId;
    private final static long serialVersionUID = -7456850511233486880L;

    public Integer getDrawValue() {
      return drawValue;
    }

    public void setDrawValue(Integer drawValue) {
      this.drawValue = drawValue;
    }

    public String getViewValue() {
      return viewValue;
    }

    public void setViewValue(String viewValue) {
      this.viewValue = viewValue;
    }

    public Integer getStyleId() {
      return styleId;
    }

    public void setStyleId(Integer styleId) {
      this.styleId = styleId;
    }

    @Override
    public String toString() {
      return "AreaPiece{" +
          "drawValue=" + drawValue +
          ", viewValue='" + viewValue + '\'' +
          ", styleId=" + styleId +
          '}';
    }
  }

  public class AreaStyle implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("objectId")
    @Expose
    private String objectId;
    private final static long serialVersionUID = -5623075380192457605L;

    public Integer getId() {
      return id;
    }

    public void setId(Integer id) {
      this.id = id;
    }

    public String getColor() {
      return color;
    }

    public void setColor(String color) {
      this.color = color;
    }

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public String getObjectId() {
      return objectId;
    }

    public void setObjectId(String objectId) {
      this.objectId = objectId;
    }

    @Override
    public String toString() {
      return "AreaStyle{" +
          "id=" + id +
          ", color='" + color + '\'' +
          ", title='" + title + '\'' +
          ", objectId='" + objectId + '\'' +
          '}';
    }
  }

  public class Data implements Serializable {

    @SerializedName("chartName")
    @Expose
    private String chartName;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("areaStyles")
    @Expose
    private List<AreaStyle> areaStyles = null;
    @SerializedName("areas")
    @Expose
    private List<Area> areas = null;
    private final static long serialVersionUID = 7445472924463770192L;

    public String getChartName() {
      return chartName;
    }

    public void setChartName(String chartName) {
      this.chartName = chartName;
    }

    public String getUnit() {
      return unit;
    }

    public void setUnit(String unit) {
      this.unit = unit;
    }

    public List<AreaStyle> getAreaStyles() {
      return areaStyles;
    }

    public void setAreaStyles(List<AreaStyle> areaStyles) {
      this.areaStyles = areaStyles;
    }

    public List<Area> getAreas() {
      return areas;
    }

    public void setAreas(List<Area> areas) {
      this.areas = areas;
    }

    @Override
    public String toString() {
      return "Data{" +
          "chartName='" + chartName + '\'' +
          ", unit='" + unit + '\'' +
          ", areaStyles=" + areaStyles +
          ", areas=" + areas +
          '}';
    }
  }
}

