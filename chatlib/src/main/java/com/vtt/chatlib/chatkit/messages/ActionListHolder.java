package com.vtt.chatlib.chatkit.messages;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.vtt.chatlib.R;

/**
 * Created by HaiKE on 10/9/17.
 */

public class ActionListHolder extends RecyclerView.ViewHolder {
  public TextView mTvTitle;//, mBtnMore;
  public RecyclerView mRecycler;
  private Context mContext;

  public ActionListHolder(View itemView, Context context) {
    super(itemView);
    mContext = context;
    mTvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
    mRecycler = (RecyclerView) itemView.findViewById(R.id.recycler);
    init();
  }

  private void init() {
    if (mContext == null)
      return;
    mRecycler.setHasFixedSize(true);
    LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
    layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
    mRecycler.setLayoutManager(layoutManager);
//        CustomDividerDecoration dividerItemDecoration = new CustomDividerDecoration(mContext, layoutManager.getOrientation());
//        dividerItemDecoration.setDrawable(mContext.getResources().getDrawable(R.drawable.white_home_divider));
//        mRecycler.addItemDecoration(dividerItemDecoration);
  }

//    public void bind(List<QuickReply> datas) {
//        if (mContext == null)
//            return;
//        ActionListAdapter adapter = new ActionListAdapter(mContext, datas);
//        mRecycler.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
//    }

}
