package com.vtt.chatlib.chatkit.messages;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vtt.chatlib.ChatBotUtils;
import com.vtt.chatlib.R;
import com.vtt.chatlib.chatkit.models.QuickReply;
import com.vtt.chatlib.network.WSRestful;
import com.vtt.chatlib.utils.Constants;

/**
 * Created by HaiKE on 10/9/17.
 */

public class ActionHolder extends RecyclerView.ViewHolder {
  public TextView tvTitle;
  public ImageView imgAction;
  public View viewContent;
  private QuickReply action;
  private Context context;

  public ActionHolder(View itemView, Context context) {
    super(itemView);
    this.context = context;
    tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
    imgAction = (ImageView) itemView.findViewById(R.id.img_action);
    viewContent = itemView.findViewById(R.id.view_content);
  }

  public void bind(QuickReply item) {
    this.action = item;
    itemView.setVisibility(View.VISIBLE);

    if (item.getContentType() != null) {
      if (item.getContentType().trim().toLowerCase().equals(Constants.TYPE_QUICK_ACTION_TEXT)) {
        tvTitle.setVisibility(View.VISIBLE);
        imgAction.setVisibility(View.GONE);
        tvTitle.setText(item.getTitle());

      } else if (item.getContentType().trim().toLowerCase().equals(Constants.TYPE_QUICK_ACTION_LIKE)) {
        imgAction.setVisibility(View.VISIBLE);
        tvTitle.setVisibility(View.GONE);

        if (item.getTitle().trim().toLowerCase().equals(Constants.TYPE_QUICK_ACTION_LIKE)) {
          imgAction.setImageResource(R.drawable.ic_like);

        } else if (item.getTitle().trim().toLowerCase().equals(Constants.TYPE_QUICK_ACTION_DISLIKE)) {
          imgAction.setImageResource(R.drawable.ic_dislike);
        }

      } else {
        itemView.setVisibility(View.GONE);
      }
    } else {
      itemView.setVisibility(View.GONE);
    }
  }

}