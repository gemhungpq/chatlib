/*******************************************************************************
 * Copyright 2016 stfalcon.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.vtt.chatlib.chatkit.models;

import com.vtt.chatlib.chatdetail.data.model.ConclusionMessage;
import com.vtt.chatlib.mbi.dto.blockInfo.InfoBlock;
import com.vtt.chatlib.mbi.dto.horizontalChart.ItemHorizontalStackRankingUnitData;
import com.vtt.chatlib.mbi.dto.lineChart.ItemLineChartServiceDTO;

import java.util.ArrayList;

/*
 * Created by troy379 on 28.03.17.
 */

/**
 * Interface used to mark messages as custom content types. For its representation see {@link MessageHolders}
 */

public interface MessageContentType extends IMessage {

  /**
   * Default media type for image message.
   */
  interface Image extends IMessage {
    String getImageUrl();
  }

  interface ImageLoading extends IMessage {
    String getImageResource();
  }

  interface BlockInfo extends IMessage {
    InfoBlock getInfoBlock();

    Object getPayload();
  }

  interface LineChart extends IMessage {
    ItemLineChartServiceDTO getItemLineChart();

    Object getPayload();
  }

  interface HorizontalRankChart extends IMessage {
    ItemHorizontalStackRankingUnitData getItemHorizontalStackRankingChart();
  }

  interface AvailableFeatureType extends IMessage {
    ArrayList<com.vtt.chatlib.chatdetail.data.model.AvailableFeature> getAvailableFeatures();
  }

  interface ConclusionType extends IMessage {
    ConclusionMessage getConclusionMessage();
  }

  interface RankingInfoType extends IMessage {
    InfoBlock getRankingInfo();

    Object getPayload();
  }

  // other default types will be here
  interface Link extends IMessage {
    String getLinkImage();

    String getLinkUrl();

    String getLinkDescription();
  }

  interface QuickReply extends IMessage {
    ArrayList<com.vtt.chatlib.chatkit.models.QuickReply> getActionList();
  }

  interface ActionList extends IMessage {
    ArrayList<com.vtt.chatlib.chatkit.models.Button> getButtonList();
  }

}
