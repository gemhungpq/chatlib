package com.vtt.chatlib.chatkit.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by HaiKE on 10/25/17.
 */

public class ChartLine {
  @SerializedName("blockId")
  @Expose
  private Integer blockId;
  @SerializedName("type")
  @Expose
  private String type;
  @SerializedName("data")
  @Expose
  private Data data;
  @SerializedName("id")
  @Expose
  private Integer id;
  private final static long serialVersionUID = -8986724527855507375L;

  public Integer getBlockId() {
    return blockId;
  }

  public void setBlockId(Integer blockId) {
    this.blockId = blockId;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "Example{" +
        "blockId=" + blockId +
        ", type='" + type + '\'' +
        ", data=" + data +
        ", id=" + id +
        '}';
  }

  public class Data implements Serializable {

    @SerializedName("chartTitle")
    @Expose
    private String chartTitle;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("lines")
    @Expose
    private List<Line> lines = null;
    @SerializedName("avglines")
    @Expose
    private Object avglines;
    private final static long serialVersionUID = 2795524471952154992L;

    public String getChartTitle() {
      return chartTitle;
    }

    public void setChartTitle(String chartTitle) {
      this.chartTitle = chartTitle;
    }

    public String getUnit() {
      return unit;
    }

    public void setUnit(String unit) {
      this.unit = unit;
    }

    public List<Line> getLines() {
      return lines;
    }

    public void setLines(List<Line> lines) {
      this.lines = lines;
    }

    public Object getAvglines() {
      return avglines;
    }

    public void setAvglines(Object avglines) {
      this.avglines = avglines;
    }

    @Override
    public String toString() {
      return "Data{" +
          "chartTitle='" + chartTitle + '\'' +
          ", unit='" + unit + '\'' +
          ", lines=" + lines +
          ", avglines=" + avglines +
          '}';
    }
  }

  public class Line implements Serializable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("average")
    @Expose
    private Boolean average;
    @SerializedName("points")
    @Expose
    private List<Point> points = null;
    private final static long serialVersionUID = -9043908217324346605L;

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public String getColor() {
      return color;
    }

    public void setColor(String color) {
      this.color = color;
    }

    public Boolean getAverage() {
      return average;
    }

    public void setAverage(Boolean average) {
      this.average = average;
    }

    public List<Point> getPoints() {
      return points;
    }

    public void setPoints(List<Point> points) {
      this.points = points;
    }

    @Override
    public String toString() {
      return "Line{" +
          "title='" + title + '\'' +
          ", color='" + color + '\'' +
          ", average=" + average +
          ", points=" + points +
          '}';
    }
  }

  public class Point implements Serializable {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("viewValue")
    @Expose
    private String viewValue;
    @SerializedName("event")
    @Expose
    private Object event;
    @SerializedName("year")
    @Expose
    private Object year;
    private final static long serialVersionUID = -2758660075071387746L;

    public String getDate() {
      return date;
    }

    public void setDate(String date) {
      this.date = date;
    }

    public Integer getValue() {
      return value;
    }

    public void setValue(Integer value) {
      this.value = value;
    }

    public String getViewValue() {
      return viewValue;
    }

    public void setViewValue(String viewValue) {
      this.viewValue = viewValue;
    }

    public Object getEvent() {
      return event;
    }

    public void setEvent(Object event) {
      this.event = event;
    }

    public Object getYear() {
      return year;
    }

    public void setYear(Object year) {
      this.year = year;
    }

    @Override
    public String toString() {
      return "Point{" +
          "date='" + date + '\'' +
          ", value=" + value +
          ", viewValue='" + viewValue + '\'' +
          ", event=" + event +
          ", year=" + year +
          '}';
    }
  }
}
