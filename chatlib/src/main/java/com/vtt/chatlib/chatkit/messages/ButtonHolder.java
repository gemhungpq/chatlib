package com.vtt.chatlib.chatkit.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.vtt.chatlib.R;
import com.vtt.chatlib.chatkit.models.Button;

/**
 * Created by HaiKE on 10/9/17.
 */

public class ButtonHolder extends RecyclerView.ViewHolder {
  public TextView tvTitle;
  public TextView tvSubtitle;
  public TextView btnSubmit;
  private Button action;
  private Context context;

  public ButtonHolder(View itemView, Context context) {
    super(itemView);
    this.context = context;
    tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
    tvSubtitle = (TextView) itemView.findViewById(R.id.tvSubTitle);
    btnSubmit = (TextView) itemView.findViewById(R.id.btnSubmit);
  }

  public void bind(Button item) {
    this.action = item;
    tvTitle.setText(item.getTitle());
    tvSubtitle.setText(item.getSubtitle());
  }
}