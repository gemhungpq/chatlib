package com.vtt.chatlib.chatkit.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by HaiKE on 10/25/17.
 */

public class ChartColumn {
  @SerializedName("blockId")
  @Expose
  private Integer blockId;
  @SerializedName("type")
  @Expose
  private String type;
  @SerializedName("data")
  @Expose
  private Data data;
  @SerializedName("id")
  @Expose
  private Integer id;
  private final static long serialVersionUID = -8986724527855507375L;

  public Integer getBlockId() {
    return blockId;
  }

  public void setBlockId(Integer blockId) {
    this.blockId = blockId;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "Example{" +
        "blockId=" + blockId +
        ", type='" + type + '\'' +
        ", data=" + data +
        ", id=" + id +
        '}';
  }

  public class Column implements Serializable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("singleColumn")
    @Expose
    private List<SingleColumn> singleColumn = null;
    private final static long serialVersionUID = 4962916321797822293L;

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public String getColor() {
      return color;
    }

    public void setColor(String color) {
      this.color = color;
    }

    public List<SingleColumn> getSingleColumn() {
      return singleColumn;
    }

    public void setSingleColumn(List<SingleColumn> singleColumn) {
      this.singleColumn = singleColumn;
    }

    @Override
    public String toString() {
      return "Column{" +
          "title='" + title + '\'' +
          ", color='" + color + '\'' +
          ", singleColumn=" + singleColumn +
          '}';
    }
  }

  public class Data implements Serializable {

    @SerializedName("chartName")
    @Expose
    private String chartName;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("columns")
    @Expose
    private List<Column> columns = null;
    private final static long serialVersionUID = -9146342397464190498L;

    public String getChartName() {
      return chartName;
    }

    public void setChartName(String chartName) {
      this.chartName = chartName;
    }

    public String getUnit() {
      return unit;
    }

    public void setUnit(String unit) {
      this.unit = unit;
    }

    public List<Column> getColumns() {
      return columns;
    }

    public void setColumns(List<Column> columns) {
      this.columns = columns;
    }

    @Override
    public String toString() {
      return "Data{" +
          "chartName='" + chartName + '\'' +
          ", unit='" + unit + '\'' +
          ", columns=" + columns +
          '}';
    }
  }

  public class SingleColumn implements Serializable {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("viewValue")
    @Expose
    private String viewValue;
    @SerializedName("event")
    @Expose
    private Object event;
    @SerializedName("year")
    @Expose
    private Object year;
    private final static long serialVersionUID = -5688261019934372137L;

    public String getDate() {
      return date;
    }

    public void setDate(String date) {
      this.date = date;
    }

    public Integer getValue() {
      return value;
    }

    public void setValue(Integer value) {
      this.value = value;
    }

    public String getViewValue() {
      return viewValue;
    }

    public void setViewValue(String viewValue) {
      this.viewValue = viewValue;
    }

    public Object getEvent() {
      return event;
    }

    public void setEvent(Object event) {
      this.event = event;
    }

    public Object getYear() {
      return year;
    }

    public void setYear(Object year) {
      this.year = year;
    }

    @Override
    public String toString() {
      return "SingleColumn{" +
          "date='" + date + '\'' +
          ", value=" + value +
          ", viewValue='" + viewValue + '\'' +
          ", event=" + event +
          ", year=" + year +
          '}';
    }
  }
}
